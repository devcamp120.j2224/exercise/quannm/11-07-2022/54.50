import javax.swing.plaf.synth.Region;
import java.util.ArrayList;

public class Country {
    String countryCode;
    String countryName;
    ArrayList<Region> regions;

    public Country(String countryCode, String countryName) {
        this.countryCode = countryCode;
        this.countryName = countryName;
    }

}
