import java.util.ArrayList;

public class Main {
    public static void main(String[] args) {
        System.out.println("Hello world!");

        ArrayList<Country> listCountry = new ArrayList<Country>();

        Country country1 = new Country("VN", "Việt Nam");
        Country country2 = new Country("USA", "Hoa Kỳ");
        Country country3 = new Country("CHN", "Trung Quốc");
        Country country4 = new Country("AUS", "Úc");


        listCountry.add(country1);
        listCountry.add(country2);
        listCountry.add(country3);
        listCountry.add(country4);
//        System.out.println(listCountry);

        for (Country country : listCountry) {
            System.out.println("Country[countryCode=" + country.countryCode + ",countryName=" + country.countryName + "]");
        }

        ArrayList<Region> listRegion = new ArrayList<Region>();

        Region region1 = new Region("HCM", "Hồ Chí Minh");
        Region region2 = new Region("HN", "Hà Nội");
        Region region3 = new Region("DN", "Đà Nẵng");

        listRegion.add(region1);
        listRegion.add(region2);
        listRegion.add(region3);

        for (Country country : listCountry) {
            if (country.countryCode == "VN") {
                for (Region region : listRegion) {
                    System.out.println("Country[countryCode=" + country.countryCode + ",countryName=" + country.countryName
                            + ",Region[regionCode=" + region.regionCode + ",regionName=" + region.regionName + "]]");
                }

            }
        }
    }
}